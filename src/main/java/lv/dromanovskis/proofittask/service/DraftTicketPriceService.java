package lv.dromanovskis.proofittask.service;

import lv.dromanovskis.proofittask.data.request.AgeGroup;
import lv.dromanovskis.proofittask.data.request.DraftTicketPriceRequest;
import lv.dromanovskis.proofittask.data.request.Traveler;
import lv.dromanovskis.proofittask.data.response.DraftTicketPriceResponse;
import lv.dromanovskis.proofittask.data.response.Position;
import lv.dromanovskis.proofittask.service.discount.DiscountException;
import lv.dromanovskis.proofittask.service.discount.DiscountGroup;
import lv.dromanovskis.proofittask.service.discount.DiscountService;
import lv.dromanovskis.proofittask.service.route.RouteRateService;
import lv.dromanovskis.proofittask.service.route.RouteRateServiceException;
import lv.dromanovskis.proofittask.service.tax.TaxService;
import lv.dromanovskis.proofittask.service.tax.TaxServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Map;

import static lv.dromanovskis.proofittask.service.discount.DiscountGroup.CHILD;
import static lv.dromanovskis.proofittask.service.discount.DiscountGroup.STANDARD;
import static lv.dromanovskis.proofittask.service.tax.TaxType.VAT;

@Service
public class DraftTicketPriceService {

    Logger log = LoggerFactory.getLogger(DraftTicketPriceService.class);

    public static final String LUGGAGE = "luggage";

    @Autowired
    TaxService taxService;
    @Autowired
    RouteRateService routeRateService;
    @Autowired
    DiscountService discountService;


    public DraftTicketPriceResponse calculateDraftTicketPrice(DraftTicketPriceRequest request)
            throws DraftPriceCalculationException {
        log.info("Processing request: {}", request);
        DraftTicketPriceResponse response = new DraftTicketPriceResponse();

        final BigDecimal standardTicketPrice;
        try {
            standardTicketPrice = routeRateService.getStandardRate(request.getBusTerminalName());
        } catch (RouteRateServiceException e) {
            DraftPriceCalculationException error = new DraftPriceCalculationException("Error in retrieving route rate", e);
            log.error("RouteRateService failed", error);
            throw error;
        }

        for (Traveler traveler : request.getTravelers()) {
            response.addPosition(
                    travelerPosition(
                            traveler.getAgeGroup(),
                            standardTicketPrice));
            if (traveler.getUnitsOfLuggage() > 0) {
                response.addPosition(
                        luggagePosition(traveler.getUnitsOfLuggage(), standardTicketPrice)
                );
            }
        }

        log.info("Draft Ticket Price response: {}", response);
        return response;
    }

    protected Position luggagePosition(int unitsOfLuggage, BigDecimal ticketPrice) throws DraftPriceCalculationException {
        BigDecimal price = ticketPrice;
        price = price.multiply(BigDecimal.valueOf(unitsOfLuggage));
        price = applyDiscount(LUGGAGE, price);
        price = applyVAT(price);
        Position position = new Position(LUGGAGE, price);
        log.debug(position.toString());
        return position;
    }

    protected Position travelerPosition(AgeGroup ageGroup, BigDecimal ticketPrice)
            throws DraftPriceCalculationException {
        BigDecimal price = applyDiscount(ageGroup, ticketPrice);
        price = applyVAT(price);
        Position position = new Position(ageGroup.name(), price);
        log.debug(position.toString());
        return position;
    }


    protected BigDecimal applyDiscount(AgeGroup ageGroup, BigDecimal inputSum) throws DiscountException {
        Map<DiscountGroup, BigDecimal> discountMultipliers = discountService.discountMultipliers();
        return inputSum.multiply(switch (ageGroup) {
            case ADULT -> discountMultipliers.get(STANDARD);
            case CHILD -> discountMultipliers.get(CHILD);
        }).stripTrailingZeros();
    }

    protected BigDecimal applyDiscount(String miscellaneous, BigDecimal inputSum) throws DiscountException {
        Map<DiscountGroup, BigDecimal> discountMultipliers = discountService.discountMultipliers();
        return inputSum.multiply(switch (miscellaneous) {
                    case LUGGAGE -> discountMultipliers.get(DiscountGroup.LUGGAGE);
                    default -> {
                        IllegalStateException error = new IllegalStateException("Unexpected value: " + miscellaneous);
                        log.error("Unexpected discount group {}", miscellaneous);
                        throw error;
                    }
                }
        ).stripTrailingZeros();
    }

    protected BigDecimal applyVAT(BigDecimal inputSum) throws DraftPriceCalculationException {
        BigDecimal multiplier;
        try {
            multiplier = BigDecimal.valueOf(
                    taxService.getTaxRate(VAT) + 100).divide(BigDecimal.valueOf(100));
        } catch (TaxServiceException e) {
            DraftPriceCalculationException error = new DraftPriceCalculationException("Error while applying VAT", e);
            log.error("Tax service failed:", error);
            throw error;
        }
        return inputSum.multiply(multiplier).stripTrailingZeros();
    }
}

package lv.dromanovskis.proofittask.service.tax;

public enum TaxType {
    VAT,
    OTHER_TAX,
    YET_ANOTHER_TAX
}

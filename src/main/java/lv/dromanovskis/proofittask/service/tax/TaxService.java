package lv.dromanovskis.proofittask.service.tax;

public interface TaxService {
    int getTaxRate(TaxType taxType) throws TaxServiceException;
}

package lv.dromanovskis.proofittask.service.tax;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

import static lv.dromanovskis.proofittask.service.tax.TaxType.*;

@Service
public class TaxServiceImplementation implements TaxService {
    @Override
    public int getTaxRate(TaxType taxType) throws TaxServiceException {
        return getTaxRates().get(taxType);
    }

    /*
        This Is a placeholder method for accessing the existing service that provides tax rates for any given day.
        Since the tax rates change on daily bases, some caching could be leveraged to reduce number of requests.
     */
    private Map<TaxType, Integer> getTaxRates() {
        HashMap<TaxType, Integer> rates = new HashMap<>();
        rates.put(VAT, 21);
        rates.put(OTHER_TAX, 5);
        rates.put(YET_ANOTHER_TAX, 75);
        return rates;
    }
}

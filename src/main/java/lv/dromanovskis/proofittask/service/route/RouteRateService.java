package lv.dromanovskis.proofittask.service.route;

import java.math.BigDecimal;

public interface RouteRateService {
    BigDecimal getStandardRate(String routeID)  throws RouteRateServiceException;
}

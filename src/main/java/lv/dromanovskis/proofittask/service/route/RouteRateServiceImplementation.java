package lv.dromanovskis.proofittask.service.route;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/*
    This class is a placeholder for a service that would retrieve standard price for
    bus routes.
    FIXME: The request should be made with some kind of route ID rather then bus terminal name. Only in case of
            requesting this information from a specific bus terminal would existing approach make sense.
 */
@Service
public class RouteRateServiceImplementation implements RouteRateService {
    @Override
    public BigDecimal getStandardRate(String busTerminalName) throws RouteRateServiceException {
        return new BigDecimal("10.00");
    }
}

package lv.dromanovskis.proofittask.service;

public class DraftPriceCalculationException extends Exception {
    public DraftPriceCalculationException(String message, Throwable cause) {
        super(message, cause);
    }
}

package lv.dromanovskis.proofittask.service.discount;

import java.math.BigDecimal;
import java.util.Map;

public interface DiscountService {
    Map<DiscountGroup, BigDecimal> discountMultipliers() throws DiscountException;
}

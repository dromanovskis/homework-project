package lv.dromanovskis.proofittask.service.discount;

public enum DiscountGroup {
    STANDARD, CHILD, LUGGAGE
}

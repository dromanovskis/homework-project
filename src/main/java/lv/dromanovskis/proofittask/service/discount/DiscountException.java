package lv.dromanovskis.proofittask.service.discount;

import lv.dromanovskis.proofittask.service.DraftPriceCalculationException;

/*
    This exception represents situation where the service we are working with returns errors in other ways than
    throwing exception.but we want to handle their errors in on our terms.
 */
public class DiscountException extends DraftPriceCalculationException {
    public DiscountException(String message, Throwable cause) {
        super(message, cause);
    }
}

package lv.dromanovskis.proofittask.service.discount;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static lv.dromanovskis.proofittask.service.discount.DiscountGroup.*;

/*
    This class is a stand-in for an already existing service that would return discount multipliers to be applied for
    different discount groups. Such information should be adjustable on-the-fly in production.
 */
@Service
public class DiscountServiceImplementation implements DiscountService {
    @Override
    public Map<DiscountGroup, BigDecimal> discountMultipliers() throws DiscountException {
        HashMap<DiscountGroup, BigDecimal> discountMultipliers = new HashMap<>();
        discountMultipliers.put(STANDARD, new BigDecimal("1.0"));
        discountMultipliers.put(CHILD, new BigDecimal("0.5"));
        discountMultipliers.put(LUGGAGE, new BigDecimal("0.3"));
        return discountMultipliers;
    }
}

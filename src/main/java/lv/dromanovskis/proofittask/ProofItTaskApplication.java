package lv.dromanovskis.proofittask;

import lv.dromanovskis.proofittask.data.request.DraftTicketPriceRequest;
import lv.dromanovskis.proofittask.data.request.Traveler;
import lv.dromanovskis.proofittask.data.response.DraftTicketPriceResponse;
import lv.dromanovskis.proofittask.service.DraftPriceCalculationException;
import lv.dromanovskis.proofittask.service.DraftTicketPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

import static lv.dromanovskis.proofittask.data.request.AgeGroup.ADULT;
import static lv.dromanovskis.proofittask.data.request.AgeGroup.CHILD;

@SpringBootApplication
public class ProofItTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProofItTaskApplication.class, args);
    }

    @Autowired
    DraftTicketPriceService draftTicketPriceService;

    @PostConstruct
    public void runAcceptanceCriteriaRequest() throws DraftPriceCalculationException {
        DraftTicketPriceRequest request = new DraftTicketPriceRequest();
        request.setBusTerminalName("Vilnius, Lithuania");
        request.addTraveler(new Traveler(ADULT, 2));
        request.addTraveler(new Traveler(CHILD, 1));

        DraftTicketPriceResponse response = draftTicketPriceService.calculateDraftTicketPrice(request);
        System.out.println(response);
    }
}

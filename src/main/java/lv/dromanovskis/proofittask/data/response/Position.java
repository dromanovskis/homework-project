package lv.dromanovskis.proofittask.data.response;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static java.lang.String.format;

public final class Position {
    private final String name;
    private final BigDecimal price;

    public Position(String name, BigDecimal price) {
        this.name = name;
        this.price = price.setScale(2, RoundingMode.UP).stripTrailingZeros();
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return BigDecimal.valueOf(price.doubleValue()).stripTrailingZeros();
    }

    @Override
    public String toString() {
        return format("Position { name = %s, price = %s}", name, price);
    }
}

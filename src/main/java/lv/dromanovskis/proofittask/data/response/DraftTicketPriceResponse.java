package lv.dromanovskis.proofittask.data.response;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DraftTicketPriceResponse {
    BigDecimal totalPrice = new BigDecimal("0");
    List<Position> breakdown = new ArrayList<>();

    protected void appendToTotalPrice(BigDecimal decimal) {
        totalPrice = totalPrice.add(decimal);
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public List<Position> getBreakdown() {
        return breakdown;
    }

    public void addPosition(Position position) {
        this.breakdown.add(position);
        appendToTotalPrice(position.getPrice());
    }

    @Override
    public String toString() {
        Function<List<Position>, String> list =
                (positions) -> positions.stream().map(Position::toString).collect(Collectors.joining(":"));

        return String.format("DraftTicketPriceResponse { totalPrice = %s ,breakdown [%s] }",
                totalPrice.toString(), list.apply(breakdown));
    }
}

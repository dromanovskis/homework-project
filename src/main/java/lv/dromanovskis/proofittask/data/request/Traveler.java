package lv.dromanovskis.proofittask.data.request;

import static java.lang.String.format;

public final class Traveler {
    private final AgeGroup ageGroup;
    private final int unitsOfLuggage;

    public Traveler(AgeGroup ageGroup, int unitsOfLuggage) {
        this.ageGroup = ageGroup;
        this.unitsOfLuggage = unitsOfLuggage;
    }

    public AgeGroup getAgeGroup() {
        return ageGroup;
    }

    public int getUnitsOfLuggage() {
        return unitsOfLuggage;
    }

    @Override
    public String toString() {
        return format("Traveler{ ageGroup = %s, unitsOfLuggage = %s}",
                ageGroup, unitsOfLuggage);
    }
}

package lv.dromanovskis.proofittask.data.request;

public enum AgeGroup {
    ADULT, CHILD
}

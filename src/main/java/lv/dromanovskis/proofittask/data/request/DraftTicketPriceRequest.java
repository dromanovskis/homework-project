package lv.dromanovskis.proofittask.data.request;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.lang.String.format;

public class DraftTicketPriceRequest {
    private String busTerminalName;
    private List<Traveler> travelers = new ArrayList<>();

    public String getBusTerminalName() {
        return busTerminalName;
    }

    public void setBusTerminalName(String busTerminalName) {
        this.busTerminalName = busTerminalName;
    }

    public List<Traveler> getTravelers() {
        return travelers;
    }

    public void setTravelers(List<Traveler> travelers) {
        this.travelers = travelers;
    }

    public void addTraveler(Traveler traveler) {
        travelers.add(traveler);
    }

    @Override
    public String toString() {
        Function<List<Traveler>, String> f = (travelers) ->
                travelers.stream().map(Traveler::toString).collect(Collectors.joining(":"));
        return format("DraftTicketPriceRequest{ busTerminalName = %s, travelers = [%s]}",
                busTerminalName, f.apply(travelers));
    }
}

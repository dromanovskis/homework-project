package lv.dromanovskis.proofittask.service;

import lv.dromanovskis.proofittask.data.request.DraftTicketPriceRequest;
import lv.dromanovskis.proofittask.data.response.Position;
import lv.dromanovskis.proofittask.service.discount.DiscountException;
import lv.dromanovskis.proofittask.service.discount.DiscountGroup;
import lv.dromanovskis.proofittask.service.discount.DiscountService;
import lv.dromanovskis.proofittask.service.discount.DiscountServiceImplementation;
import lv.dromanovskis.proofittask.service.route.RouteRateService;
import lv.dromanovskis.proofittask.service.route.RouteRateServiceException;
import lv.dromanovskis.proofittask.service.tax.TaxService;
import lv.dromanovskis.proofittask.service.tax.TaxServiceException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import static lv.dromanovskis.proofittask.data.request.AgeGroup.ADULT;
import static lv.dromanovskis.proofittask.data.request.AgeGroup.CHILD;
import static lv.dromanovskis.proofittask.service.discount.DiscountGroup.LUGGAGE;
import static lv.dromanovskis.proofittask.service.discount.DiscountGroup.STANDARD;
import static lv.dromanovskis.proofittask.service.tax.TaxType.VAT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DraftTicketPriceServiceTest {
    @InjectMocks
    DraftTicketPriceService service = new DraftTicketPriceService();
    @Mock
    TaxService taxService;
    @Mock
    RouteRateService routeRateService;
    @Spy
    DiscountService discountService = new DiscountServiceImplementation();

    @Test
    public void shouldRethrowRouteRateException() throws RouteRateServiceException {
        DraftTicketPriceRequest request = new DraftTicketPriceRequest();
        request.setBusTerminalName("Moria");
        when(routeRateService.getStandardRate(anyString())).thenThrow(new RouteRateServiceException());
        assertThrows(DraftPriceCalculationException.class, () -> service.calculateDraftTicketPrice(request));
    }

    private static Stream<Arguments> vatTestParameters() {
        return Stream.of(
                Arguments.of(new BigDecimal("0.0"), new BigDecimal("0")),
                Arguments.of(new BigDecimal("100.0"), new BigDecimal("1.5E+2")),
                Arguments.of(new BigDecimal("100.5"), new BigDecimal("150.75")),
                Arguments.of(new BigDecimal("0.01"), new BigDecimal("0.015"))
        );
    }

    @ParameterizedTest
    @MethodSource("vatTestParameters")
    public void vatShouldBeAppliedCorrectly(BigDecimal input, BigDecimal expected)
            throws TaxServiceException, DraftPriceCalculationException {
        when(taxService.getTaxRate(VAT)).thenReturn(50);

        BigDecimal output = service.applyVAT(input);
        assertEquals(expected, output);
    }

    @Test
    public void vatApplyingShouldRethrowException()
            throws TaxServiceException {
        when(taxService.getTaxRate(VAT)).thenThrow(new TaxServiceException());
        assertThrows(DraftPriceCalculationException.class, () -> service.applyVAT(new BigDecimal("42")));
    }

    @Test
    public void discountToAdultsShouldBeApplied() throws DiscountException {
        when(discountService.discountMultipliers()).thenReturn(discounts());
        BigDecimal output = service.applyDiscount(ADULT, new BigDecimal("1.0"));
        assertEquals(new BigDecimal("1"), output);
    }

    @Test
    public void discountToChildShouldBeApplied() throws DiscountException {
        when(discountService.discountMultipliers()).thenReturn(discounts());
        BigDecimal output = service.applyDiscount(CHILD, new BigDecimal("1.0"));
        assertEquals(new BigDecimal("0.5"), output);
    }

    @Test
    public void discountTo1LuggageShouldBeApplied() throws DiscountException {
        when(discountService.discountMultipliers()).thenReturn(discounts());
        BigDecimal output = service.applyDiscount(DraftTicketPriceService.LUGGAGE, new BigDecimal("1.0"));
        assertEquals(new BigDecimal("0.1"), output);
    }

    @Test
    public void shouldThrowExceptionForUnknownDiscountKey() throws DiscountException {
        when(discountService.discountMultipliers()).thenReturn(discounts());
        assertThrows(IllegalStateException.class,
                () -> service.applyDiscount("futurama", new BigDecimal("1.0")));

    }

    @Test
    public void travelerPositionShouldApplyDiscountAndVat() throws TaxServiceException, DraftPriceCalculationException {
        when(taxService.getTaxRate(VAT)).thenReturn(50);
        when(discountService.discountMultipliers()).thenReturn(discounts());

        Position position = service.travelerPosition(CHILD, new BigDecimal("10"));
        assertEquals(CHILD.name(), position.getName());
        assertEquals(new BigDecimal("7.5"), position.getPrice());
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2})
    public void positionShouldMultiplyByAmountAndApplyDiscountAndVat(int amount)
            throws DraftPriceCalculationException, TaxServiceException {
        when(taxService.getTaxRate(VAT)).thenReturn(50);
        when(discountService.discountMultipliers()).thenReturn(discounts());

        Position position = service.luggagePosition(amount, new BigDecimal("10"));

        assertEquals(DraftTicketPriceService.LUGGAGE, position.getName());
        assertEquals(
                new BigDecimal("1.5").multiply(BigDecimal.valueOf(amount)).stripTrailingZeros(),
                position.getPrice());
    }

    private Map<DiscountGroup, BigDecimal> discounts() {
        Map<DiscountGroup, BigDecimal> discounts = new HashMap<>();
        discounts.put(STANDARD, new BigDecimal("1"));
        discounts.put(DiscountGroup.CHILD, new BigDecimal("0.5"));
        discounts.put(LUGGAGE, new BigDecimal("0.1"));
        return discounts;
    }
}
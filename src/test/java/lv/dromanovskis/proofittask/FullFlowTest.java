package lv.dromanovskis.proofittask;

import lv.dromanovskis.proofittask.data.request.DraftTicketPriceRequest;
import lv.dromanovskis.proofittask.data.request.Traveler;
import lv.dromanovskis.proofittask.data.response.DraftTicketPriceResponse;
import lv.dromanovskis.proofittask.data.response.Position;
import lv.dromanovskis.proofittask.service.DraftPriceCalculationException;
import lv.dromanovskis.proofittask.service.DraftTicketPriceService;
import lv.dromanovskis.proofittask.service.discount.DiscountService;
import lv.dromanovskis.proofittask.service.discount.DiscountServiceImplementation;
import lv.dromanovskis.proofittask.service.route.RouteRateService;
import lv.dromanovskis.proofittask.service.route.RouteRateServiceException;
import lv.dromanovskis.proofittask.service.tax.TaxService;
import lv.dromanovskis.proofittask.service.tax.TaxServiceException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

import static lv.dromanovskis.proofittask.data.request.AgeGroup.ADULT;
import static lv.dromanovskis.proofittask.data.request.AgeGroup.CHILD;
import static lv.dromanovskis.proofittask.service.DraftTicketPriceService.LUGGAGE;
import static lv.dromanovskis.proofittask.service.tax.TaxType.VAT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FullFlowTest {
    @InjectMocks
    DraftTicketPriceService service = new DraftTicketPriceService();
    @Mock
    TaxService taxService;
    @Mock
    RouteRateService routeRateService;
    @Spy
    DiscountService discountService = new DiscountServiceImplementation();


    @Test
    public void shouldComplete() throws DraftPriceCalculationException, RouteRateServiceException, TaxServiceException {
        when(taxService.getTaxRate(VAT)).thenReturn(21);
        when(routeRateService.getStandardRate(anyString())).thenReturn(new BigDecimal(10));
        DraftTicketPriceRequest request = new DraftTicketPriceRequest();
        request.setBusTerminalName("Vilnius, Lithuania");
        request.addTraveler(new Traveler(ADULT, 2));
        request.addTraveler(new Traveler(CHILD, 1));

        DraftTicketPriceResponse response = service.calculateDraftTicketPrice(request);

        assertEquals(new BigDecimal("29.04"), response.getTotalPrice());
        assertEquals(4, response.getBreakdown().size());

        Position position = response.getBreakdown().get(0);
        assertEquals(ADULT.name(), position.getName());
        assertEquals(new BigDecimal("12.1"), position.getPrice());

        position = response.getBreakdown().get(1);
        assertEquals(LUGGAGE, position.getName());
        assertEquals(new BigDecimal("7.26"), position.getPrice());

        position = response.getBreakdown().get(2);
        assertEquals(CHILD.name(), position.getName());
        assertEquals(new BigDecimal("6.05"), position.getPrice());

        position = response.getBreakdown().get(3);
        assertEquals(LUGGAGE, position.getName());
        assertEquals(new BigDecimal("3.63"), position.getPrice());
    }
}

package lv.dromanovskis.proofittask.data.response;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class PositionTest {
    @Test
    public void shouldRoundTo2Decimals(){
        Position position = new Position("name",new BigDecimal("0.001"));
        assertEquals(new BigDecimal("0.01"), position.getPrice());
    }
}